<?php

namespace App\Providers;

use Rollbar\Rollbar;
use Illuminate\Support\ServiceProvider;


class ErrorHandlingServiceProvider extends ServiceProvider{

  public function register(){

    $config = [
      'access_token' => getenv('ROLLBAR_TOKEN'),
      'environment' => getenv('APP_ENV'),
    ];

    Rollbar::init($config);


  }


}