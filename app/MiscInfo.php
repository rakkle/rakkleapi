<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiscInfo extends Model{

  /**
   * Table associated with model
   * @var string
   */
  protected $table = 'miscinfo';

  /**
   * Values to be edited
   * @var array
   */
  protected $fillable = ['views', 'likes', 'reports'];

  /**
   * Dates within models
   * @var array
   */
  protected $dates = [
    'updated_at', 'created_at', 'deleted_at'
  ];

  /**
   * Relationship with PetList
   */
  protected function PetList(){
    $this->belongsTo('App\PetList');
  }


}