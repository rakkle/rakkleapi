<?php

namespace App\Http\Controllers;

use App\User;
use CTL\JWTBase;
use App\PetList;
use App\Company;
use CTL\CTLImageFactory;
use App\PetListingImages;
use Illuminate\Http\Request;

class PetListingController extends Controller{


  /**
   * Json Web Token interface
   */
  protected $jwt;

  /**
   * Image Factory interface
   */
  protected $imgfactory;


  public function __construct(JWTBase $jwt, CTLImageFactory $imgfactory){
    $this->jwt = $jwt;
    $this->middleware('auth');
    $this->imgfactory = $imgfactory;
  }

  /**
   * Showing all Pet Listing on DB
   * @return json
   */
  public function index(Request $request){
    $listings = PetList::all();

    if($listings->isEmpty() ){
      return response()->json([
        'error' => [
          'message' => 'No Pets Listed',
          'code' => '5'
        ]
      ], 404);
    }

      $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $getToken = json_decode(json_encode($token['getRequest']), true);
      if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }
      return response()->json([
            'data' => [
              'listings' => $listings->toArray()
            ]
          ], 200);

  }

  /**
   * Showing all User Pet listing
   * @return array
   */
  public function showManyListings($user_id, Request $request){
    $userListings = PetList::usersListing($user_id)->get();
    if( $userListings->isEmpty() ){
      return response()->json([
          'error' => [
              'message'=>'No Listing found from user '.$user_id,
              'code' => '5'
          ]
      ], 404);
    }
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $getToken = json_decode(json_encode($token['getRequest']), true);
      if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

    return response()->json([
      'data' => [
          'listings' => $userListings->toArray()
      ]
    ], 200);
  }

  /**
   * Showing A user pet listing
   * @param  int $user_id
   * @param  int $id
   * @return array
   */
  public function showUserListing($user_id, $id, Request $request){
    $singleUserListing = PetList::usersListing($user_id)->singleList($id)->get();

    if( $singleUserListing->isEmpty() ){
      return response()->json([
          'error' => [
              'message'=>'User id '. $user_id.' does not have listing '. $id,
              'code' => '5'
          ]
      ], 404);
    }

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $getToken = json_decode(json_encode($token['getRequest']), true);
      if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

    return response()->json([
      'data' => [
          'listing' => $singleUserListing->toArray()
      ]
    ], 200);
  }

  /**
   * Showing all Company Pet listings
   * @param  int $company_id
   * @return array
   */
  public function showManyCompanyListings($company_id, Request $request){
    $compListings = PetList::companyListing($company_id)->get();
    if($compListings->isEmpty() ){
      return response()->json([
          'error' => [
              'message'=>'Company id '.$company_id.' does not have any listings',
              'code' => '5'
          ]
      ], 404);
    }

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $getToken = json_decode(json_encode($token['getRequest']), true);
      if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

    return response()->json([
      'data' => [
          'listing' => $compListings->toArray()
      ]
    ], 200);
  }

  /**
   * Showing A company pet listing
   * @param  int $company_id
   * @param  int $id
   * @return array
   */
  public function showCompanyListing($company_id, $id, Request $request){
    $singleCompListing = PetList::companyListing($company_id)->singleList($id)->get();
    if($singleCompListing->isEmpty() ){
      return response()->json([
          'error' => [
              'message'=> 'Company id '. $company_id . ' does not have listing '. $id,
              'code' => '5'
          ]
      ], 404);
    }

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $getToken = json_decode(json_encode($token['getRequest']), true);
      if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

    return response()->json([
      'data' => [
          'listings' => $singleCompListing->toArray()
      ]
    ], 200);
  }

  /**
   * Posting New User Pet Listing
   * @param  int $user_id
   * @param Request $request
   * @return void
   */
  public function postUserListing($user_id, Request $request){
    $user = User::find($user_id);

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));

    $newpost = json_decode(json_encode($token['listing']), true);

    $user->PetList()->create($newpost);

    return response()->json([
      'success' => [
        'message' => 'Successfuly Posted Listing',
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Posting New Company Pet Listing
   * @param  int  $company_id
   * @param  Request $request
   * @return void
   */
  public function postCompanyListing($company_id, Request $request){
    $comp = Company::find($company_id);

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );

    $newpost = json_decode(json_encode($token['listing']), true);


    $comp->PetList()->create($newpost);

    return response()->json([
      'success' => [
        'message' => 'Successfuly Posted Listing',
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Post new Pet list Images
   * @param  int  $listing_id
   * @param  Request $request
   * @return void
   */
  public function postListingImages($listing_id, Request $request){
    //Get posted listing
    $listing = PetList::find($listing_id);

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );

    $newImage = json_decode(json_encode($token['listing_image']), true);

    $this->Validate($request,[
        'PetListImages' => 'required|max:3000|mimes:jpg,jpeg,png',
    ]);

    if($newImage !== 'newImage'.$listing_id){
      return response()->json([
          'error' => [
            'message' => 'You do not have permission to view this resource',
            'code' => '1'
          ]
        ], 403);
    }

    $imageUpload = $this->imgfactory->petlistingImgUpload($request->file('PetListImages'), $listing_id);

    $listing->PetListingImages()->create([
      'pet_list_id' => $listing_id,
      'img_name' => $imageUpload,
      'img' => $imageUpload,
    ]);

    return response()->json([
      'success' => [
        'message' => 'Successfuly Posted Image',
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Updating Company Pet Listing
   * @param  int  $company_id
   * @param  int  $id
   * @param  Request $request
   * @return void
   */
  public function patchCompanyListing($company_id, $id, Request $request){

    $listing = PetList::companyListing($company_id)->singleList($id)->first();

      $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
      $updatepost = json_decode(json_encode($token['listing']), true);

      $listing->update($updatepost);

      return response()->json([
      'success' => [
        'message' => 'Successfuly Updated Resourse id '. $id,
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Updating User Pet Listing
   * @param  int  $user_id
   * @param  int  $id
   * @param  Request $request
   * @return void
   */
  public function patchUserListing($user_id, $id, Request $request){
    $listing = PetList::usersListing($user_id)->singleList($id)->first();

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
    $updatepost = json_decode(json_encode($token['listing']), true);

    $listing->update($updatepost);

    return response()->json([
      'success' => [
        'message' => 'Successfuly Updated Resourse id '. $id,
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Updates A Pet listing Image
   * @param  int  $id
   * @param  Request $request
   * @return void
   */
  public function destroyListingImages($id, $listing_id, Request $request){
    $listing = PetListingImages::find($id);

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );

    $deleteImg = json_decode(json_encode($token['delete_image']), true);

    if($deleteImg !== 'delete'.$id){
      return response()->json([
        'error' => [
          'message' => 'You either do not have permission to delete listing id '.$id.' or missing credentials to remove resource',
          'code' => '1'
        ]
      ],401);
    }

    $listing->delete();

    return response()->json([
      'success' => [
        'message' => 'Successfuly Deleted Image',
        'code' => '10'
      ]
    ], 200);

  }


  /**
   * Soft Deletes User Pet Listing
   * @param  int  $user_id
   * @param  int  $id
   * @param  Request $request
   * @return void
   */
  public function destroyUserListing($user_id, $id, Request $request){
    $listing = PetList::usersListing($user_id)->singleList($id)->first();

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
    $deletelisting = json_decode(json_encode($token['deleteListing']), true);

      if('delete'.$id !== $deletelisting){
          return response()->json([
            'error' => [
              'message' => 'You either do not have permission to delete listing id '.$id.' or missing credentials to remove resource',
              'code' => '1'
            ]
          ],401);
      }

    $listing->delete();
    // $listing->PetListingImages()->get()->delete(); // Needs Attention

    return response()->json([
      'success' => [
        'message' => 'Successfuly Deleted Resourse id '. $id,
        'code' => '10'
      ]
    ], 200);
  }

  /**
   * Soft Deletes Company Pet Listing
   * @param  int  $company_id
   * @param  int  $id
   * @param  Request $request
   * @return void
   */
  public function destroyCompanyListing($company_id, $id, Request $request){

    $listing = PetList::companyListing($company_id)->singleList($id)->first();

    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
    $deletelisting = json_decode(json_encode($token['deleteListing']), true);
      if('delete'.$id !== $deletelisting){
          return response()->json([
            'error' => [
              'message' => 'You either do not have permission to delete listing id '.$id.' or missing credentials to remove resource',
              'code' => '1'
            ]
          ],401);
      }

    $listing->delete();
    // $listing->PetListingImages()->get()->delete(); // Needs Attention

    return response()->json([
      'success' => [
        'message' => 'Successfuly Deleted Resourse id '. $id,
        'code' => '10'
      ]
    ], 200);
  }


  /**
   * Creates and updates Views, Likes and Reports on pet listings
   * @param  int  $listing_id
   * @param  Request $request
   * @return void
   */
  public function miscInfoAddition($listing_id, Request $request){
   $listing = PetList::find($listing_id);

   $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
   $miscinfo = json_decode(json_encode($token['ListingVote']), true);


   count($listing->MiscInfo()->get()) == 0 ? $listing->MiscInfo()->create($miscinfo) : $listing->MiscInfo()->update($miscinfo);

   return response()->json([
      'success' => [
        'message' => 'Successfuly Updated',
        'code' => '10'
      ]
    ], 200);

  }


}