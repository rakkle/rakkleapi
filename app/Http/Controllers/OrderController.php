<?php

namespace App\Http\Controllers;

use App\Order;
use App\PetList;
use CTL\JWTBase;
use Illuminate\Http\Request;

class OrderController extends Controller{


  protected $jwt;


  public function __construct(JWTBase $jwt){
    $this->jwt = $jwt;
    $this->middleware('auth');
  }

  /**
   * Shows all Sellers Orders
   * @param  int $seller_id
   * @param  Requst $request
   * @return array
   */
  public function getSellerOrders($seller_id, Request $request){
    // Collect and decrypt token
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
    $getOrders = json_decode(json_encode($token['getOrders']), true);
    if($getOrders !== 'getOrders'.$seller_id){
      return response()->json([
        'error' => [
          'message' => 'You do not have permission to view these resources',
          'code' => '1'
        ]
      ],403);
    }
    // Get Sellers Orders
    $orders = Order::usersPetSold($seller_id)->get();
    if(empty($orders)){
      return response()->json([
        'info' => [
          'message' => 'No Orders have been made',
        ]
      ], 404);
    }

    // return 200
    return response()->json([
      'data' => [
        'Orders' => $orders->toArray()
      ]
    ], 200);

  }



  /**
   * Shows All Customers Orders
   * @param  int $customer_id
   * @param  Requst $request
   * @return array
   */
  public function getCustomersOrders($customer_id, Request $request){
    // Collect and decrypt token
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
    $getOrders = json_decode(json_encode($token['getOrders']), true);
    if($getOrders !== 'getOrders'.$customer_id){
      return response()->json([
        'error' => [
          'message' => 'You do not have permission to view these resources',
          'code' => '1'
        ]
      ],403);
    }

    // Get Sellers Orders
    $orders = Order::customersPetPurchased($customer_id)->get();
    // dd($orders);
    if(empty($orders)){
      return response()->json([
        'info' => [
          'message' => 'No Orders have been made',
        ]
      ], 404);
    }

    // return 200
    return response()->json([
      'data' => [
        'Orders' => $orders->toArray()
      ]
    ], 200);

  }


  /**
   * Post New Orders and updates sellers listing
   * @param  int  $listing_id
   * @param  Request $request
   * @return void
   */
  public function postSellerOrder($listing_id, Request $request){
    //Collect and decrypt token
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
    $orderData = json_decode(json_encode($token['orderData']), true);
    $updateWasSold = json_decode(json_encode($token['WasSold']), true);

    if($updateWasSold !== '1'){
      return response()->json([
        'error' => [
          'message' => 'An error occurred when trying to make a purchase',
          'code' => '1'
        ]
      ],403);
    }

    //Post new order
    Order::create($orderData);

    // Find listing
    $newOrder = PetList::find($listing_id);

    // Soft Delete Listing and update was_sold on Petlisting
    $newOrder->update([
      "was_sold" => $updateWasSold
      ]);
    $newOrder->delete();

    //return 200
    return response()->json([
      'success' => [
        'message' => 'You Order has been Placed Successfuly',
        'code' => '10'
      ]
    ], 200);
  }





}