<?php

namespace App\Http\Controllers;

use App\User;
use CTL\JWTBase;
use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller{

    protected $jwt;


    /**
     * Construct method
     */
    public function __construct(JWTBase $jwt){
        $this->jwt = $jwt;
        $this->middleware('auth');
    }

    /**
     * Shows all Companies
     * @return array
     */
    public function index(Request $request){
      $company = Company::all();

      if(!$company){
            return response()->json([
                'error' => [
                    'message'=>'No Users loaded',
                    'code' => '1'
                ]
            ], 404);
        }

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['getRequest']), true);
        if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }


        return response()->json([
            'data' => [
                'user' => $company->toArray()
            ]
        ], 200);
    }

    /**
     * Shows A Company
     * @param  int $id
     * @return array
     */
    public function show($id, Request $request){
      $company = Company::find($id);

      if(!$company){
            return response()->json([
                'error' =>[
                    'message' => 'Company not found',
                    'code' => '2'
                ]
            ], 404);
        }

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['getRequest']), true);
        if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }


        return response()->json([
            'data' => [
                'user' => $company->toArray()
            ]
        ], 200);
    }


    public function create($id, Request $request){
        // Find user
        $user = User::find($id);
        // Collect and decrypt token companyRegister
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $companyRegister = json_decode(json_encode($token['companyRegister']), true);

        // Call relationship and create company
        try{
            $user->Company()->create($companyRegister);
            return response()->json([
              'success' => [
                'message' => 'Company Created Successfuly by User '. $id,
                'code' => '10'
              ]
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'error' =>[
                    'message' => 'Error in Creating Company',
                    'info' => 'Company name or email could be already in use',
                    'code' => '9'
                ]
            ], 500);
        }
    }



    /**
     * Updates Company Information
     * @return void
     */
    public function update($id, Request $request){
        $user = Company::find($id);

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $updateCompany = json_decode(json_encode($token['updateCompany']), true);

        $user->update($updateCompany);
        return response()->json([
          'success' => [
            'message' => 'Successfuly Updated Company '.$id,
            'code' => '10'
          ]
        ], 200);
    }

    /**
     * Removes Company
     * @return void
     */
    public function destroy($id, Request $request){
        $user = Company::find($id);

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $deleteCompany = json_decode(json_encode($token['deleteCompany']), true);
        if('delete'.$id !== $deleteCompany){
              return response()->json([
                'error' => [
                  'message' => 'You either do not have permission to delete company id '.$id.' or missing credentials to remove user',
                  'code' => '1'
                ]
              ],401);
        }


        $user->destroy($id);

        return response()->json([
            'success' => [
                'message' => 'Company '.$id.' been removed',
                'code' => '10'
            ]
        ], 200);
    }

    /**
     * Shows All companies with listings
     * @param  Request $request
     * @return array
     */
    public function getCompanyPetListing(Request $request){
      $companyPetListing = Company::allCompanyPetListing()->get();

      if($companyPetListing->isEmpty()){
        return response()->json([
        'error' => [
            'message' => 'No Companies with Pet Listing Can be shown',
            'code' =>'5'
          ]
        ], 404);
      }

      return response()->json([
        'data' => [
          'listing' => $companyPetListing->toArray()
        ]
      ], 200);
    }



}