<?php

namespace App\Http\Controllers;

use CTL\JWTBase;
use CTL\PetReview;
use Illuminate\Http\Request;

class ReviewController extends Controller
{

    public $review;

    public $jwt;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PetReview $review, JWTBase $jwt)
    {
        $this->review = $review;
        $this->jwt = $jwt;
    }

    /**
     * [getPetReviews description]
     * @param  [type]  $petlistid [description]
     * @param  Request $request   [description]
     * @return [type]             [description]
     */
    public function getPetReviews($petlistid, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $showReview = json_decode(json_encode($token['getReviews']), true);

        if($showReview !== 'getReviews'){
            return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

        $reviews = $this->review->getPetReview($petlistid);
        // dd($reviews);

        return response()->json([
            'data' => [
                'reviews' => $reviews
            ]
        ], 200);
    }

    /**
     * [getCompanyReviews description]
     * @param  [type]  $company_id [description]
     * @param  Request $request    [description]
     * @return [type]              [description]
     */
    public function getCompanyReviews($company_id, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $showReview = json_decode(json_encode($token['getReviews']), true);

        if($showReview !== 'getReviews'){
            return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

        $reviews = $this->review->getCompanyReview($company_id);

        return response()->json([
            'data' => [
                'reviews' => $reviews
            ]
        ], 200);

    }

    /**
     * [getReviewComments description]
     * @param  [type]  $petlistid [description]
     * @param  Request $request   [description]
     * @return [type]             [description]
     */
    public function getReviewComments($petlistid, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $showReview = json_decode(json_encode($token['getReviews']), true);

        if($showReview !== 'getReviews'){
            return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }


        $reviews = $this->review->getReviewComments($petlistid);

        return response()->json([
            'data' => [
                'reviews' => $reviews
            ]
        ], 200);

    }


    /**
     * [postPetReview description]
     * @param  [type]  $petlistid [description]
     * @param  Request $request   [description]
     * @return [type]             [description]
     */
    public function postPetReview($petlistid, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $getReview = json_decode(json_encode($token['reviews']), true);

        $this->review->postPetReview($petlistid, $getReview);

        return response()->json([
            'success' => [
                'message' => 'Review Submitted',
                'code' => '10'
            ]
        ], 200);
    }

    /**
     * [postCompanyReview description]
     * @param  [type]  $company_id [description]
     * @param  Request $request    [description]
     * @return [type]              [description]
     */
    public function postCompanyReview($company_id, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $getReview = json_decode(json_encode($token['reviews']), true);

        $this->review->postCompanyReview($company_id, $getReview);

        return response()->json([
            'success' => [
                'message' => 'Company Review Submitted',
                'code' => '10'
            ]
        ], 200);
    }

    /**
     * [postReviewComment description]
     * @param  [type]  $petlistid [description]
     * @param  Request $request   [description]
     * @return [type]             [description]
     */
    public function postReviewComment($petlistid, Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token'));
        $getComment = json_decode(json_encode($token['reviewComment']), true);

        $this->review->postReviewComments($petlistid, $getComment);

        return response()->json([
            'success' => [
                'message' => 'Comment Submitted',
                'code' => '10'
            ]
        ], 200);
    }


}
