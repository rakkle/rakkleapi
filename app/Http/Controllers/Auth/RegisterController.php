<?php

namespace App\Http\Controllers\Auth;

use App\User;
use CTL\JWTBase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{

    protected $jwt;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JWTBase $jwt)
    {
        $this->jwt = $jwt;
    }


    /**
     * Registers New User
     * @param  Request $request
     * @param  User    $user
     * @return void
     */
    public function register(Request $request, User $user){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $register = json_decode(json_encode($token['register']), true);

        try{
            User::create([
                'username' => $register['username'],
                'email' => $register['email'],
                'first_name' => $register['first_name'],
                'last_name' => $register['last_name'],
                'dp_image' => $register['dp_image'],
                'password' => password_hash($register['password'], PASSWORD_BCRYPT)
            ]);


            return response()->json([
              'success' => [
                'message' => 'User Created Successfuly',
                'code' => '10',
              ]
            ], 200);
        }catch(\Exception $e){
            return response()->json([
                'error' =>[
                    'message' => 'Error in Creating User',
                    'info' => 'username or email already registered',
                    'code' => '9'
                ]
            ], 500);
        }
    }


    /**
     * Registers oauth User
     * @param  Request $request
     * @return array
     */
    public function oauthUserRegister(Request $request){
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['createUser']), true);


        if($getToken !== 'createUser'){
                User::oauthRegisterUser([
                    'user_id' => $getToken['user_id'],
                    'email' => $getToken['email'],
                    'facebook_id' => $getToken['facebook_id'],
                    'google_id' => $getToken['google_id']
                ]);
                User::create([
                    'email' => $getToken['email'],
                    'first_name' => $getToken['first_name'],
                    'last_name' => $getToken['last_name'],
                    'dp_image' => $getToken['dp_image'],
                ]);
                return response()->json([
                  'success' => [
                    'message' => 'User Created Successfuly',
                    'code' => '10',
                  ]
                ], 200);
        }

        return response()->json([
                'error' =>[
                    'message' => 'Error in Creating User',
                    'info' => 'Please try registering another way',
                    'code' => '9'
                ]
            ], 500);



    }


}