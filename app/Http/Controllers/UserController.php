<?php

namespace App\Http\Controllers;

use App\User;
use CTL\JWTBase;
use CTL\CTLImageFactory;
use Illuminate\Http\Request;

class UserController extends Controller
{

    protected $jwt;

    protected $imgfactory;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(JWTBase $jwt, CTLImageFactory $imgfactory)
    {
        $this->jwt = $jwt;
        $this->middleware('auth');
        $this->imgfactory = $imgfactory;
    }

    /**
     * All users response
     * @return array
     */
    public function index(Request $request){
        $users =  User::all();

        if(!$users){
            return response()->json([
                'error' => [
                    'message'=>'No Users loaded',
                    'code' => '1'
                ]
            ], 404);
        }

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['getRequest']), true);
        if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }


        return response()->json([
            'data' => [
                'user' => $users->toArray()
            ]
        ], 200);
    }

    /**
     * Specific user response
     * @return array
     */
    public function show($id, Request $request){
        $users = User::find($id);

        if(!$users){
            return response()->json([
                'error' =>[
                    'message' => 'User not found',
                    'code' => '1'
                ]
            ], 404);
        }

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['getRequest']), true);
        if($getToken !== 'getRequest'){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

        return response()->json([
            'data' => [
                'user' => $users->toArray()
            ]
        ], 200);
    }

    /**
     * Updates Users Information
     * @return void
     */
    public function update($id, Request $request){
        // Find specific user
        $user = User::find($id);
        // Collect and decrypt token
        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $updateUser = json_decode(json_encode($token['updateUser']), true);
        // Check if user has rights to update info
        $user->update($updateUser);
        // update info
        return response()->json([
          'success' => [
            'message' => 'Successfuly Updated User '. $id,
            'code' => '10'
          ]
        ], 200);
    }

    /**
     * Deleting Specific User Response
     * @return [type] [description]
     */
    public function destroy($id, Request $request){

        $user = User::find($id);

        $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $deleteUser = json_decode(json_encode($token['deleteUser']), true);
        if('delete'.$id !== $deleteUser){
              return response()->json([
                'error' => [
                  'message' => 'You either do not have permission to delete user id '.$id.' or missing credentials to remove user',
                  'code' => '1'
                ]
              ],401);
        }


        $user->destroy($id);

        return response()->json([
            'success' => [
                'message' => 'User has '.$id.' been removed',
                'code' => '10'
            ]
        ], 200);

    }

    /**
     * Uploads user avatar
     * @param  int  $id
     * @param  Request $request
     * @return array
     */
    public function avatarUpload($id, Request $request){
      $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );
        $getToken = json_decode(json_encode($token['avatarUpload']), true);
        $user = User::find($id);

        if($getToken !== 'avatarUpload'.$id){
          return response()->json([
            'error' => [
              'message' => 'You do not have permission to view this resource',
              'code' => '1'
            ]
          ], 403);
        }

        $this->Validate($request,[
            'avatar' => 'required|max:3000|mimes:jpg,jpeg,png',
        ]);
        $imageUpload = $this->imgfactory->userAvatarUpload($request->file('avatar'), $id);

        $user->update([
          'dp_image' => $imageUpload
        ]);


        return response()->json([
          'success' => [
            'message' => 'User '. $id .' Avatar successfully upload',
            'AvatarURL' => $imageUpload,
            'code' => '10'
          ]
        ], 200);


    }


}
