<?php

namespace App\Http\Controllers;

use App\User;
use App\PetList;
use CTL\JWTBase;
use CTL\SearchBase;
use App\PetListingImages;
use Illuminate\Http\Request;

class SearchController extends Controller{

  protected $jwt;

  protected $searchfilter;

  /**
   * @param JWTBase $jwt
   * @param SearchBase  $search
   */
  public function __construct(JWTBase $jwt, SearchBase $searchfilter){
    $this->jwt = $jwt;
    $this->searchfilter = $searchfilter;
    // $this->middleware('auth');
  }

  /**
   * Presents users with defined search query results
   * @param  Request $request
   * @param  PetList $petlist
   * @return array
   */
  public function search(Request $request, PetList $petlist ){
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );


    $search = $this->searchfilter->filter($token)->get();


    if($search->isEmpty() ){
      return response()->json([
          'error' => [
            'message' => 'Not Found, Try other search queries',
            'code' => '30'
          ]
        ], 404);
    }else{
    return response()->json([
      'data' => [
        'results' => $search->toArray()
      ]
    ], 200);
  }


  }

  /**
   * Elastic Search content on database
   * @param  Request $request
   * @param  PetList $petlist
   * @return array
   */
  public function elastic(Request $request, PetList $petlist){
    $token = $this->jwt->parseJWTClaim($request->header('x-access-token') );

    $search = json_decode(json_encode($token['elasticSearch']), true);

    $searchResult = $petlist->search($search)->get();


      if($searchResult->isEmpty() ){
      return response()->json([
          'error' => [
            'message' => 'Not Found, Try other search queries',
            'code' => '30'
          ]
        ], 404);
      }else{
      return response()->json([
        'data' => [
          'results' => $searchResult->toArray()
        ]
      ], 200);
    }


  }



}