<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * Table used by the Model
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'first_name', 'last_name', 'user_location', 'dp_image', 'password', 'user_gender'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * Declaring date attributes
     * @var [type]
     */
    protected $dates = [
        'created_at', 'updated_at'
    ];

    /**
     * Relationship Declaration
     *
     */
    public function Company(){
        return $this->hasOne('App\Company');
    }

    /**
     * Relationship Declaration
     */
    public function PetList(){
        return $this->HasMany('App\PetList');
    }


    /**
     * Returns Valued username
     * @param  [type] $query [description]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function scopeUsername($query, $value){
        return $query->where('username', $value)->first();
    }

    /**
     * Returns Valued Email
     * @param  object $query
     * @param  string $value
     * @return string
     */
    public function scopeEmail($query, $value){
        return $query->where('email', $value)->first();
    }

    /**
     * Saves oauth User info
     * @param  array  $value
     * @return void
     */
    public function oauthRegisterUser($value = []){
        \DB::table('oauthUser')->insert($value);
    }


}
