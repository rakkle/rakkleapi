<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PetListingImages extends Model{

  protected $table = 'petlist_images';

  protected $fillable = ['pet_list_id', 'img_name', 'img' ];

  protected $dates = [
    'updated_at', 'created_at', 'deleted_at'
  ];

  public function PetList(){
    return $this->belongsTo('App\PetList');
  }

}