<?php

namespace App\Console\Commands;

use App\User;
use CTL\JWTBase;
use Illuminate\Console\Command;

class CreateJWToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jwt:create {uid} {payload}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new JWT Token';

    /**
     * The JWT Token instance.
     *
     * @var JWTBase
     */
    protected $jwt;

    /**
     * Create a new command instance.
     *
     * @param  JWTBase  $jwt
     * @return void
     */
    public function __construct(JWTBase $jwt)
    {
        parent::__construct();

        $this->jwt = $jwt;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $createdtoken = $this->jwt->buildJWTtoken($this->argument('uid'), $this->argument('payload'));

       $this->info('<comment>Created Token Successfuly:</comment>');
       $this->info($createdtoken);
    }
}