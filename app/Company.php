<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model{

  /**
   * [$table description]
   *
   */
  protected $table = 'company';

  /**
   * [$fillable description]
   *
   */
  protected $fillable = [
    'name', 'phone_number', 'email', 'company_location', 'lat_long', 'company_dp_image', 'user_created_id', 'desc', 'company_rating', 'company_reviews'
  ];

  /**
   * [$dates description]
   *
   */
  protected $dates = [
    'updated_at', 'created_at'
  ];

  /**
   * Relationship declaration
   * @return [type] [description]
   */
  public function user(){
    return $this->belongsTo('App\User');
  }

  public function PetList(){
    return $this->HasMany('App\PetList');
  }

  public function scopeAllCompanyPetListing($query){
    return $query->whereNotNull('id')->with('PetList');
  }


}