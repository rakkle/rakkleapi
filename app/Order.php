<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model{


  protected $table = 'orders_list';

  protected $fillable = [
    'pet_listing_id', 'seller_id', 'customer_id', 'total_paid', 'grand_total'
  ];

  protected $dates = [
    'updated_at', 'created_at'
  ];


  /**
   * [user description]
   * @return [type] [description]
   */
  public function user(){
    return $this->belongsTo('App\User');
  }

  /**
   * [petlisting description]
   * @return [type] [description]
   */
  public function petlisting(){
    return $this->belongsTo('App\PetList');
  }

  /**
   * [scopeUsersPetSold description]
   * @return [type] [description]
   */
  public function scopeUsersPetSold($query, $seller_id){
    return $query->where('seller_id', $seller_id); //->with('PetListingImages');
  }

  /**
   * [scopeCustomersPetPurchased description]
   * @return [type] [description]
   */
  public function scopeCustomersPetPurchased($query, $customer_id){
    return $query->where('customer_id', $customer_id); //->with('PetListingImages');
  }




}