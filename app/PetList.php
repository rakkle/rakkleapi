<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PetList extends Model{

  use SoftDeletes, Searchable;

  /**
   * [$table description]
   * @var string
   */
  protected $table = 'pet_listings';

  /**
   * [$fillable description]
   * @var [type]
   */
  protected $fillable = [
    'user_id', 'company_id',
    'was_sold', 'pet_name',
    'price', 'pet_sex',
    'pet_dob','pet_type', 'pet_breed',
    'pet_size', 'status', 'bio', 'for_adoption', 'pet_category'
  ];

  /**
   * [$dates description]
   * @var [type]
   */
  protected $dates = [
    'updated_at', 'created_at', 'deleted_at'
  ];

  /**
   * Eager Loads Model with every request
   * @var [type]
   */
  protected $with = [
    'MiscInfo', 'PetListingImages'
  ];

  /**
   * Relationship with User model
   * @return object
   */
  public function user(){
    return $this->belongsTo('App\User');
  }

  /**
   * Relationship with Company model
   */
  public function Company(){
    return $this->belongsTo('App\Company');
  }

  /**
   * Relationship with pet_listings_images
   */
  public function PetListingImages(){
    return $this->HasMany('App\PetListingImages');
  }

  /**
   * Relationship with MiscInfo
   */
  public function MiscInfo(){
    return $this->HasOne('App\MiscInfo');
  }

  /**
   * Queries User id input,
   * @param  object $query
   * @param  int $id
   * @return array
   */
  public function scopeUsersListing($query, $id){
    return $query->where('user_id', $id)->with('PetListingImages');
  }

  /**
   * Queries Company id input
   * @param  object $query
   * @param  int    $id
   * @return array
   */
  public function scopeCompanyListing($query, $id){
    return $query->where('company_id', $id)->with('PetListingImages');
  }

  /**
   * Query returns a single listing
   * @param  object $query
   * @param  int    $id
   * @return array
   */
  public function scopeSingleList($query, $id){
    return $query->where('id', $id)->with('PetListingImages');
  }



}