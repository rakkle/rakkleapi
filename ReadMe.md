# Rakkle Services
[ ![Codeship Status for rakkle/rakkleapi](https://app.codeship.com/projects/3db38da0-fe0c-0134-56ba-5274708b3ee2/status?branch=master)](https://app.codeship.com/projects/212202)
[![Software License](https://img.shields.io/badge/License-EPL-green.svg?style=flat-square)](License.md)

## Rakkle API Service Routes
[More here](API.md)


##License

Rakkle Services software is under a [Proprietary License Agreement](License.md)