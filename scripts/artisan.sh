#! /bin/bash

echo '>>> Setting up env'

sudo chmod -R 777 /var/www
sudo chmod -R 777 storage/*
sudo chmod -R 777 bootstrap/*
sudo cp .env.example .env


echo ">>> Migrate Database"

sudo php artisan migrate:install
sudo php artisan migrate