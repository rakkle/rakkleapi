# Welcome to Rakkle Mobile API Services

## JWT Tokens
All Rakkle Service API routes are middleware protected. You can view the [Class here](https://bitbucket.org/rakkle/rakkleapi/src/27b8f747ce8603cd20767c9005fccfa24d4642ed/app/Http/Middleware/Authenticate.php?at=master&fileviewer=file-view-default)
You must pass a [JWT](http://jwt.io) token to get the appropriate response from the desired route

JWT Claim names as follow:

- `GET` (Used for all get request) = `getRequest`
- `POST` (For Pet Listings only) = `listing`
- `POST` (For Pet Listing Images only) = `listing_image`
- `PATCH` (For Pet Listings only) = `listing`
- `PATCH` (For Pet Listing Images only) = `listing_image`
- `DELETE` (For Removing Pet Listings) = `deleteListing`
- `POST` (For Orders only) = `orderData`
- `POST` (To be added within the order token) = `WasSold`
- `GET` (For Orders only) = `getOrders`


The items that **SHOULD BE** in the JWT Claims are as follow:

- `"getRequest" : "getRequest"`
- `"listing": array[]`
- `"listing_image": array[]`
- `"deleteListing": "delete{id}"` `{id}` represents the id of the resource you want to delete.
- `"orderData": array[]`
- `"WasSold": "1"`
- `"getOrders": "getOrders{seller_id}"` {seller_id} represents the id of the user





## Routes

| Verb | Route | Parameters Accepted | Extra Info|
|------|:-------|:-------------------|:----------|
| | Authentication Routes|
| POST |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/login]()  | `"username" :"username"`, `"password" :"password"`| N/A |
| POST |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/register]() | `"register": ["email": "{value}", "username": "{value}", "first_name" : "{value}", "last_name" : "{value}", "dp_image" : "{value}", "password": "{value}"  ]`| `password` and `dp_image` are nullable values. `email` and `username` are unique values.
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/oauthUser]()| `"user_id": "{value}","first_name":"{value}","last_name":"{value}","email": "{value}","facebook_id": "{value}","google_id": "{value}","dp_image":"{value}"`| `user_id` is a foreign key for user. `facebook_id` and `google_id` are both nullable values but don't accept dublicate values. `email` is the associated email with google or facebook |
| | User Routes|
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/users/]() | `"getRequest" : "getRequest"`| N/A|
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/user/{id}]()  | `"getRequest" : "getRequest"` | Gets individual user|
| PATCH |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/user/{id}]()  | `"updateUser": ["email": "{value}", "username": "{value}", "first_name" : "{value}", "last_name" : "{value}", "password": "{value}", "user_gender":"{value}"  ]` | N/A   |
| DELETE |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/user/{id}]()  | `"deleteUser" : "delete{id}"` | N/A |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/user/avatar/{id}]() | `"avatarUpload":"avatarUpload{id}"` | `id` is the `userid` of the user associated with the avatar. In uploading an image you would have to send it through the body with the paramater `avatar`. This route any also be used to update a users avatar |
| | Company  Routes |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/company]()  | `"getRequest" : "getRequest"` | N/A |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/company/{id}]() |  `"getRequest" : "getRequest"` | N/A |
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/company/petlist]() | `"getRequest" : "getRequest"`| Gets all companies and their pet listings|
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/company/{id}]() | `"companyRegister": ["name":"{value}", "phone_number":"{value}", "company_location":"{value}", "lat_long":"{value}", "email":"{value}", "company_dp_image":"{value}", "desc":"{value}", "company_rating":"{value}", "company_reviews": "{value}"]`  | `name` and `email` should be unique values  |
| PATCH |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/company/{id}]() | `"updateCompany": ["name":"{value}", "phone_number":"{value}", "company_location":"{value}", "lat_long":"{value}", "email":"{value}", "company_dp_image":"{value}", "desc":"{value}", "company_rating":"{value}", "company_reviews": "{value}"]` |  `company_rating`, `company_location` and `company_reviews` accept nullable values |
| DELETE|[http://rakkleservicesapi.rakkle.com/index.php/api/v1/company/{id}]() | `"deleteCompany": "delete{id}"`  |  N/A |
| | Pet Listing GET Routes |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/listings/all]() | `"getRequest" : "getRequest"` | N/A |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/get/{user_id}]() | `"getRequest" : "getRequest"` | N/A |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/get/company/{company_id}]() | `"getRequest" : "getRequest"` | N/A |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/get/{user_id}/{id}]() |  `"getRequest" : "getRequest"` | N/A |
| GET |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/get/company/{company_id}/{id}]() |  `"getRequest" : "getRequest"` | N/A |
| | Pet Listing Post Routes |
| POST |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/post/user/{user_id}]() | `"listing": ["pet_listing_name" : "{value}", "was_sold":"0", "pet_name":"{value}", "price": "99999.00", "pet_sex": "{value}", "pet_location": "{value}", "pet_dob":"{value}", "pet_breed": "{value}", "pet_type":"{value}", "pet_weight": "{value}", "pet_size": "{value}", "for_adoption": "0", "status":"{value}", "bio": "{value}", "pet_category": "{value}"]`| `pet_name`, `pet_breed`, `pet_type`, `pet_weight`, `pet_size` are nullable values. `was_sold` and `for_adoption` are boolean value. `price` is a float value |
| POST |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/post/company/{company_id}]() | `"listing": ["pet_listing_name" : "{value}", "was_sold":"0", "pet_name":"{value}", "price": "99999.00", "pet_sex": "{value}", "pet_location": "{value}", "pet_dob":"{value}", "pet_breed": "{value}", "pet_type":"{value}", "pet_weight": "{value}", "pet_size": "{value}", "for_adoption": "0", "status":"{value}", "bio": "{value}", "pet_category": "{value}"]`| `pet_name`, `pet_breed`, `pet_type`, `pet_weight`, `pet_size` are nullable values. `was_sold` and `for_adoption` are boolean value. `price` is a float value |
| POST |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/post/images/{listing_id}]() | `"listing_image": "newImage{listing_id}"`| `listing_id` is used to authenticate the posting for the particular pet listing. Would need to pass a parameter `PetListImages` within the body  |
| | Pet Listing Update Routes|
| PATCH |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/update/company/{company_id}/{id}]() | `"listing": ["pet_listing_name" : "{value}", "was_sold":"0", "pet_name":"{value}", "price": "99999.00", "pet_sex": "{value}", "pet_location": "{value}", "pet_dob":"{value}", "pet_breed": "{value}", "pet_type":"{value}", "pet_weight": "{value}", "pet_size": "{value}", "for_adoption": "0", "status":"{value}", "bio": "{value}"]` | N/A |
| PATCH |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/update/user/{user_id}/{id}]() | `"listing": ["pet_listing_name" : "{value}", "was_sold":"0", "pet_name":"{value}", "price": "99999.00", "pet_sex": "{value}", "pet_location": "{value}", "pet_dob":"{value}", "pet_breed": "{value}", "pet_type":"{value}", "pet_weight": "{value}", "pet_size": "{value}", "for_adoption": "0", "status":"{value}", "bio": "{value}", "pet_category": "{value}"]` | N/A |
| PATCH | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/update/vote/{listing_id}]() | `"ListingVote": ["views": "{value}", "likes":"{value}", "reports":"{value}"]` | `views`, `likes` and reports are all nullable and `int` values |
| | Pet Listing Delete Routes |
| DELETE |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/remove/user/{user_id}/{id}]() | `"deleteListing" : "delete{id}"`  | N/A |
| DELETE |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/remove/company/{company_id}/{id}]() | `"deleteListing" : "delete{id}"` | N/A  |
| DELETE |[http://rakkleservicesapi.rakkle.com/index.php/api/v1/list/remove/images/{listing_id}/{id}]() | `"delete_image": "delete{id}"` | `id` is the image id   |
| | Order Routes|
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/orders/seller/{seller_id}]() | `"getOrders" : "getOrders{id}"`  | N/A |
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/orders/customer/{customer_id}]() | `"getOrders" : "getOrders{id}"` | N/A |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/order/{listing_id}]() |  `"WasSold": "1", "orderData": ["pet_listing_id": "{listing_id}", "seller_id":"{seller_id}", "customer_id": "{customer_id}", "total_paid": "{value}", "grand_total": "{value}" ]`| `seller_id` and `customer_id` must only be from a user record. No companies are allowed to make purchases   |
| | Search Routes |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/search]() | `"pet_listing_name": "{value}", "pet_name": "{value}", "priceMin": "{value}", "priceMax": "{value}", "pet_sex": "{value}", "pet_location": "{value}", "pet_dob_from":"{value}", "pet_dob_to": "{value}", "pet_type": "{value}", "pet_bread": "{value}", "pet_weight": "{value}", "pet_size":"{value}"` | All values can be nullable. `priceMin`, `priceMax` and `pet_dob_from`, `pet_dob_to` uses values that are within range to return values |
| POST |  [http://rakkleservicesapi.rakkle.com/index.php/api/v1/elasticsearch]()  | `"elasticSearch": "{value}"` | N/A |
| | Review Routes|
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/review/get/{petlistid}]() | `"getReviews":"getReviews"` | N/A |
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/companyReview/post/{company_id}]() | `"getReviews":"getReviews"` | N/A |
| GET | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/review/comment/post/{petlistid}]() | `"getReviews":"getReviews"` | N/A |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/review/post/{petlistid}]() | `"reviews": ["review_id":"{value}","username": "{value}","user_id": "{value}","review_postdate": "{value}","petlist_id": "{value}", "user_icon":"{value}", "review_body": "{value}","review_stars": "{value}"]` | `review_stars` should be an `int` |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/companyReview/post/{company_id}]() | `"reviews": [ "company_review_id":"{value}","username": "{value}","user_id": "{value}","review_postdate": "{value}","company_id": "value","review_body": "{value}", "user_icon":"{value}", "review_stars": "{value}"]` | `review_stars` should be an `int` |
| POST | [http://rakkleservicesapi.rakkle.com/index.php/api/v1/review/comment/post/{petlistid}]() | `"reviewComment": [ "comment_id": "{value}","username": "{value}","user_id": "{value}", "user_icon":"{value}", "comment_postdate": "{value}","review_id": "{value}","comment_body": "{value}"]` | `review_id` is associative to the review |



## Building JWT Token

In building A JWT Token to be parsed to this service, it needs to have the following:

- Issuer: rakkleservice
- Audience: http://rakkle.io

All tokens should be sent through HTTP headers `x-access-token`.
