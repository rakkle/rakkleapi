<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function(Blueprint $up){
            $up->increments('id');
            $up->string('name')->unique();
            $up->string('phone_number');
            $up->string('company_location')->nullable();
            $up->double('lat_long')->nullable();
            $up->string('email')->unique();
            $up->string('company_dp_image');
            $up->integer('user_created_id')->unsigned();
            $up->string('desc');
            $up->float('company_rating')->nullable();
            $up->integer('company_reviews')->nullable();
            $up->timestamps();

            $up->foreign('user_created_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company', function(Blueprint $up){

            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('company');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');

        });
    }
}
