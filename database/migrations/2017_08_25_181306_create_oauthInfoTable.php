<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauthInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauthUser', function(Blueprint $up){
            $up->increments('id');
            $up->integer('user_id')->unsigned();
            $up->string('email')->unique();
            $up->integer('facebook_id')->unique()->nullable();
            $up->integer('google_id')->unique()->nullable();

            $up->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oauthUser', function(Blueprint $up){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('oauthUser');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
