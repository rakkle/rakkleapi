<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetListingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_listings', function(Blueprint $up){
            $up->increments('id');
            $up->integer('user_id')->unsigned()->nullable();
            $up->integer('company_id')->unsigned()->nullable();
            $up->boolean('was_sold');
            $up->string('pet_name')->nullable();
            $up->float('price');
            $up->string('pet_sex');
            $up->string('pet_dob');
            $up->string('pet_breed')->nullable();
            $up->string('pet_type')->nullable();
            $up->string('pet_size')->nullable();
            $up->boolean('for_adoption');
            $up->string('status');
            $up->string('pet_category');
            $up->string('bio');
            $up->timestamps();
            $up->softDeletes();

            // Foreign keys not added
            $up->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $up->foreign('company_id')
                ->references('id')
                ->on('company')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pet_listings', function(Blueprint $up){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('pet_listings');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
