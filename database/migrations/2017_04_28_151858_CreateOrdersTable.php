<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_list', function(Blueprint $up){
            $up->increments('id');
            $up->integer('pet_listing_id')->unsigned();
            $up->integer('seller_id')->unsigned();
            $up->integer('customer_id')->unsigned();
            $up->float('total_paid');
            $up->float('grand_total');
            $up->timestamps();

            $up->foreign('pet_listing_id')
                ->references('id')
                ->on('pet_listings')
                ->onDelete('cascade');

            $up->foreign('seller_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $up->foreign('customer_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_list', function(Blueprint $up){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('orders_list');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
