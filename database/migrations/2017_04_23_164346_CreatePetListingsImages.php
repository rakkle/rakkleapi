<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetListingsImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petlist_images', function(Blueprint $up){
            $up->increments('id');
            $up->integer('pet_list_id')->unsigned();
            $up->string('img_name');
            $up->string('img');
            $up->timestamps();
            $up->softDeletes();

            $up->foreign('pet_list_id')
                ->references('id')
                ->on('pet_listings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('petlist_images', function(Blueprint $up){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('petlist_images');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
