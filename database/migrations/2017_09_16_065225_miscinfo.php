<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Miscinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('miscinfo', function(Blueprint $up){
            $up->increments('id');
            $up->integer('pet_list_id')->unsigned();
            $up->integer('views');
            $up->integer('likes');
            $up->integer('reports');
            $up->timestamps();
            $up->softDeletes();

            $up->foreign('pet_list_id')
                ->references('id')
                ->on('pet_listings')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('miscinfo', function(Blueprint $up){
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            Schema::drop('miscinfo');
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        });
    }
}
