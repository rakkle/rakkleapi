<?php


use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\PetListingImages as PetImg;


class PetlistingImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker::create();
        foreach(range(1, 700) as $index){

          PetImg::create([
            'pet_list_id' => $faker->numberBetween($min = 1, $max = 100),
            'img_name' => time().'_img.jpg',
            'img' => $faker->imageUrl($width = 500, $height = 500, 'animals'),
          ]);

        }
    }
}


