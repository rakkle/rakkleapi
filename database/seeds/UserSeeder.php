<?php
use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,100) as $index){
            User::create([
              'email' => $faker->email,
              'username' => $faker->username,
              'first_name' => $faker->firstName,
              'last_name' => $faker->lastName,
              'user_location' => $faker->city.','.$faker->state.' '.$faker->postcode,
              'dp_image' => $faker->imageUrl($width = 500, $height = 500, 'animals'),
              'password' => password_hash('password', PASSWORD_BCRYPT),
              'user_gender' => $faker->randomElement($array = ['Male', 'Female']),
            ]);
        }

    }


}
