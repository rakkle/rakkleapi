<?php

use App\PetList;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PetlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1, 500) as $index){
          PetList::create([
            'user_id'=> $faker->numberBetween($min = 1, $max = 100),
            'company_id' => $faker->numberBetween($min = 1, $max = 70),
            'was_sold' => $faker->boolean,
            'pet_name' => $faker->username,
            'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = NULL),
            'pet_sex' => $faker->randomElement($array = ['Male', 'Female']),
            'pet_dob' => $faker->monthName.', '.$faker->dayOfMonth.' '.$faker->year,
            'pet_breed' => $faker->randomElement($array = ['--', 'pitbull', 'poodle']),
            'pet_type' => $faker->randomElement($array = ['Cat', 'Dog']),
            'pet_size' => $faker->randomElement($array = ['Small', 'Large', 'Medium']),
            'for_adoption' => $faker->boolean,
            'status' => $faker->randomElement($array = ['Inactive', 'Available', 'Adopted', 'Sold']),
            'bio' => $faker->realText($maxNbChars = 50),
            'pet_category' => $faker->randomElement($array = ['--', '---']),
          ]);

        }
    }

}
