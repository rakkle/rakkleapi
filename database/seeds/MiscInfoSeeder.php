<?php

use App\MiscInfo;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class MiscInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1, 500) as $index){
          MiscInfo::create([
            'pet_list_id' => $faker->numberBetween($min = 1, $max = 500),
            'views' => $faker->numberBetween($min =1, $max = 99),
            'likes' => $faker->numberBetween($min =1, $max = 99),
            'reports' => $faker->numberBetween($min =1, $max = 99),
          ]);
        }
    }
}
