<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{

  protected $tables = ['users','company', 'pet_listings','petlist_images', 'order_list', 'miscinfo'];

  protected $seeders = ['UserSeeder','CompanySeeder', 'PetlistSeeder', 'PetlistingImagesSeeder', 'OrdersSeeder', 'MiscInfoSeeder'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->seeders as $seedClasses){
            $this->call($seedClasses);
        }
    }



}
