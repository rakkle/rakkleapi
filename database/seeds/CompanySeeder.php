<?php
use App\Company;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,70) as $index){
            Company::create([
              'name' => $faker->company,
              'phone_number' => $faker->phoneNumber,
              'email' => $faker->email,
              'company_location' => $faker->city.','.$faker->state.' '.$faker->postcode,
              'lat_long' => $faker->latitude($min =-90, $max=90).','.$faker->longitude($min=-180, $max =180),
              'user_created_id' => $faker->numberBetween($min = 1, $max = 10),
              'company_dp_image' => $faker->imageUrl($width = 500, $height = 500, 'cats'),
              'desc' => $faker->realText($maxNbChars = 200),
              'company_rating' => $faker->numberBetween($min = 1, $max = 4.5),
              'company_reviews' => $faker->numberBetween($min = 1, $max = 100)
            ]);
        }
    }
}
