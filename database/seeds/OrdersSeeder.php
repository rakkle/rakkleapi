<?php

use App\Order;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1, 10) as $index){
          Order::create([
            'pet_listing_id' => $faker->numberBetween($min = 1, $max = 10),
            'seller_id' => $faker->numberBetween($min = 1, $max = 100),
            'customer_id' => $faker->numberBetween($min = 1, $max = 100),
            'total_paid' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 4),
            'grand_total' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 4),
          ]);
        }
    }
}
