<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Using http://localhost.com/api/v1/{values}
 * Version One
 */
$app->group(['prefix' => 'api/v1'], function($app){

  /**
   * Auth
   */
  $app->post('login', 'Auth\LoginController@login');
  $app->post('register', 'Auth\RegisterController@register'); //Creates new user
  $app->post('oauthUser', 'Auth\RegisterController@oauthUserRegister');

  /**
   * User
   */
  $app->get('/users', 'UserController@index');
  $app->get('/user/{id}', 'UserController@show');
  $app->patch('/user/{id}', 'UserController@update');
  $app->delete('/user/{id}' , 'UserController@destroy');

  // User Avatar upload
  $app->post('/user/avatar/{id}', 'UserController@avatarUpload');

  /**
   * Company
   */
  $app->get('/company/petlist', 'CompanyController@getCompanyPetListing');
  $app->get('/company', 'CompanyController@index');
  $app->get('/company/{id}', 'CompanyController@show');
  //post
  $app->post('/company/{id}', 'CompanyController@create');
  $app->patch('/company/{id}', 'CompanyController@update');
  $app->delete('/company/{id}', 'CompanyController@destroy');

  /**
   * PetListings
   */
  $app->get('/listings/all', 'PetListingController@index');
  $app->get('/list/get/{user_id}', 'PetListingController@showManyListings');
  $app->get('/list/get/company/{company_id}', 'PetListingController@showManyCompanyListings');
  $app->get('/list/get/{user_id}/{id}', 'PetListingController@showUserListing');
  $app->get('/list/get/company/{company_id}/{id}', 'PetListingController@showCompanyListing');


  //post
  $app->post('/list/post/user/{user_id}', 'PetListingController@postUserListing');
  $app->post('/list/post/company/{company_id}', 'PetListingController@postCompanyListing');
  //Images
  $app->post('/list/post/images/{listing_id}', 'PetListingController@postListingImages');

  //Patch/Updating
  $app->patch('/list/update/company/{company_id}/{id}', 'PetListingController@patchCompanyListing');
  $app->patch('/list/update/user/{user_id}/{id}', 'PetListingController@patchUserListing');


  //Delete
  $app->delete('/list/remove/images/{listing_id}/{id}', 'PetListingController@destroyListingImages');

  $app->delete('/list/remove/user/{user_id}/{id}', 'PetListingController@destroyUserListing');

  $app->delete('list/remove/company/{company_id}/{id}', 'PetListingController@destroyCompanyListing');

  $app->patch('/list/update/vote/{listing_id}', 'PetListingController@miscInfoAddition');

  /**
   * Search based on listings
   */
  $app->post('/search', 'SearchController@search');
  $app->post('/elasticsearch', 'SearchController@elastic');


  /**
   * Orders
   */

  $app->get('/orders/seller/{seller_id}', 'OrderController@getSellerOrders');
  $app->get('/orders/customer/{customer_id}', 'OrderController@getCustomersOrders');
  $app->post('/order/{listing_id}', 'OrderController@postSellerOrder');

  /**
   * Reviews
   */

  $app->post('/review/post/{petlistid}', 'ReviewController@postPetReview');
  $app->post('/companyReview/post/{company_id}','ReviewController@postCompanyReview');
  $app->post('/review/comment/post/{petlistid}', 'ReviewController@postReviewComment');

  $app->get('/review/get/{petlistid}', 'ReviewController@getPetReviews');
  $app->get('/companyReview/get/{company_id}', 'ReviewController@getCompanyReviews');
  $app->get('/review/comment/get/{petlistid}', 'ReviewController@getReviewComments');



});

