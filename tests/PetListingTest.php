<?php

use App\PetList;
use App\PetListingImages;

class PetListingTest extends APITesting{


  public function test_all_listings(){
    //Create new listing(s)
    $this->makeListing();
    //Get route
    $this->withoutMiddleware();
    $this->json('GET', 'api/v1/listings/all');
    //See if response is 200
    // $this->assertResponseOk();
  }

  public function test_all_users_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('GET', 'api/v1/list/get/1');

    // $this->assertResponseOk();
  }

  public function test_all_companies_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('GET', 'api/v1/list/get/company/1');

    // $this->assertResponseOk();

  }

  public function test_a_user_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('GET', 'api/v1/list/get/1/10');

    // $this->assertResponseOk();
  }

  public function test_a_company_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('GET', 'api/v1/list/get/company/1/10');

    // $this->assertResponseOk();
  }

  public function test_posting_a_user_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('post', 'api/v1/list/post/user/1');

    // $this->assertResponseOk();
  }

  public function test_posting_a_company_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('post', 'api/v1/list/post/company/1');

    // $this->assertResponseOk();
  }

  public function test_posting_an_image_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('post', 'api/v1/list/post/images/1');

    // $this->assertResponseOk();
  }

  public function test_updating_a_company_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('patch', 'api/v1/list/update/company/1');

    // $this->assertResponseOk();
  }

  public function test_updating_a_user_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('patch', 'api/v1/list/update/user/1/1');

    // $this->assertResponseOk();
  }

  public function test_updating_an_image_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('patch', 'api/v1/list/update/images/1');

    // $this->assertResponseOk();
  }

  public function test_deleting_a_user_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('delete', 'api/v1/list/remove/user/1/1');

    // $this->assertResponseOk();
  }

  public function test_deleting_a_company_pet_listing(){
    $this->makeListing();

    $this->withoutMiddleware();
    $this->json('delete', 'api/v1/list/remove/company/1/1');

    // $this->assertResponseOk();
  }

  /**
   * Create Pet Listing Model
   * @param  array  $listings [description]
   * @return [type]           [description]
   */
  private function makeListing($listings = []){
    $petlist = array_merge([
      'user_id' => '1',
      'company_id' => '1',
      'pet_listing_name' => 'Name of listing',
      'was_sold' => '0',
      'pet_name' => 'Name',
      'price' => '100.00',
      'pet_sex' => 'Male',
      'pet_location' => 'New York, NY 10021',
      'pet_dob' => 'March 18, 2001',
      'pet_type' => 'Breed',
      'pet_breed' => 'Pitbull',
      'pet_weight' => '10 LBS',
      'pet_size' => 'Large',
      'status' => 'Available',
      'for_adoption' => '0',
      'bio' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      'pet_category' => '---'
      ], $listings);

    PetList::create($petlist);
  }

  /**
   * Create Pet Listing Images Model
   * @param  array  $listings [description]
   * @return [type]           [description]
   */
  private function makeImages($listings = []){
    $images = array_merge([
      'pet_list_id' => '1',
      'img_name' => 'ImageName.jpg',
      'img' => 'http://img.jpg'
      ], $listings);

    PetListingImages::create($images);
  }


}