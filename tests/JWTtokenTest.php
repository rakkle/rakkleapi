<?php

use App\User;
use CTL\JWTBase;


class JWTtokenTest extends APITesting{


  /**
   * Test JWT Token Creation
   * @return boolean
   */
  public function test_token_creation(){
    $jwt = new JWTBase;
    $token = $this->userDetailedToken();

    $this->assertTrue(isset($token));
  }

  public function test_token_claim(){
    $jwt = new JWTBase;
    $token = $this->createToken();

    $this->assertNotNull($jwt->parseJWTClaim($token));
  }

  /**
   * Test Token Validation
   * @return boolean
   */
  public function test_token_validation(){
    $jwt = new JWTBase;
    $token = $this->createToken();

    $this->assertTrue($jwt->validateJWTtoken($token) );
  }

  // public function test_saving_token(){
  //   $jwt = new JWTBase;

  //   $savingToken = $jwt->saveToken('jwtToken:1', $this->createToken() );
  //   var_dump($savingToken);

  //   $this->assertTrue($savingToken);
  // }

  /**
   * Creating JWT Token
   * @return object
   */
  private function createToken(){
    $jwt = new JWTBase;

    return $jwt->buildJWTtoken(1, $this->userDetailedToken() );
  }

  /**
   * Creating User Info
   * @return array
   */
  private function userDetailedToken(){

    $user = array_merge([
      'username' => $this->faker->username,
      'email' => $this->faker->email
    ]);

    return $user;
  }

}