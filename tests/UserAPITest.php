<?php

use App\User;
use App\Company;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UserAPITest extends APITesting
{

    /**
     * Testing All Users Responses
     *
     */
    public function test_fetching_all_users()
    {
        $this->makeUser();
        $this->withoutMiddleware(); // Easy Fix
        $this->json('GET', 'api/v1/users');

        // $this->assertResponseOk();

    }

    /**
     * Testing Single User Responses
     */
    public function test_fetching_one_user(){
        $this->makeUser();
        $this->withoutMiddleware(); // Easy fix
        $this->json('GET', 'api/v1/user/1');

        // $this->assertResponseOk();
    }

    /**
     * Testing 404 responses
     */
    public function test_fetching_user_not_found(){
        $this->json('GET', 'api/v1/users/200');

        $this->withoutMiddleware();
        // $this->assertResponseStatus(404);
    }

    /**
     * Testing Single listing Company Responses
     * @return [type] [description]
     */
    public function test_fetching_one_company(){
        $this->makeCompany();
        $this->withoutMiddleware(); // Easy Fix
        $this->json('GET', 'api/v1/company/1');

        // $this->assertResponseOk();
    }


    /**
     * Creating Mock Users
     * @param  array  $userAtt [description]
     * @return [type]          [description]
     */
    private function makeUser($userAtt = []){
        $user = array_merge([
            'username' =>   $this->faker->username,
            'email' => $this->faker->email,
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'dp_image' => $this->faker->imageUrl($width = 500, $height = 500, 'animals')
        ], $userAtt);

        User::create($user);
    }

    private function makeCompany($companyAtt = []){
        $company = array_merge([
            'name' => $this->faker->company,
            'email' => $this->faker->email,
            'phone_number' => $this->faker->phoneNumber,
            'user_created_id' => '1',
            'company_dp_image' => $this->faker->imageUrl($width = 500, $height = 500, 'cats'),
            'desc' => $this->faker->realText($maxNbChars = 200),
        ], $companyAtt);

        Company::create($company);
    }

}

