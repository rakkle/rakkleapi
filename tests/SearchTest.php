<?php

use CTL\SearchBase;


class SearchTest extends APITesting{

  /**
   * Test search filter
   * @return array
   */
  public function test_search_filter(){
    $search = new SearchBase;

    $this->assertNotNull($search->filter($this->searchFilterDetailedToken() ) );
  }

  /**
   * Creating search Info
   * @return array
   */
  private function searchFilterDetailedToken(){

    $search = array_merge([
        "pet_listing_name" => $this->faker->name ,
        "pet_name" => $this->faker->username ,
        "priceMin" => "",//$this->faker->randomFloat($nbMaxDecimals = 2, $min = 0.00, $max = NULL) ,
        "priceMax" => "", //$this->faker->randomFloat($nbMaxDecimals = 2, $min = NULL, $max = 99999.99) ,
        "pet_sex" => $this->faker->randomElement($array = ['Male', 'Female']) ,
        "pet_location" => "", //$this->faker->city.','.$this->faker->state.' '.$this->faker->postcode ,
        "pet_dob_from" => "", //$this->faker->monthName.', '.$this->faker->dayOfMonth.' '.$this->faker->year ,
        "pet_dob_to" => "",  //$this->faker->monthName.', '.$this->faker->dayOfMonth.' '.$this->faker->year ,
        "pet_type" => $this->faker->randomElement($array = ['Cat', 'Dog']) ,
        "pet_breed" => $this->faker->randomElement($array = ['--', 'pitbull', 'poodle']) ,
        "pet_weight" => "", //$this->faker-> ,
        "pet_size" => $this->faker->randomElement($array = ['Small', 'Large', 'Medium'])
    ]);


    return $search;
  }



  // EOF
}