<?php

return [
  /**
   *
   */
'default' => env('ELASTIC_CONNECTION'),

'connections' => [
  'default' => [
      'servers' => [
          [
              "host" => env('ELASTIC_HOST'),
              "port" => env('ELASTIC_PORT'),
              'user' => env('ELASTIC_USER'),
              'pass' => env('ELASTIC_PASS'),
              'scheme' => env('ELASTIC_SCHEME'),
          ]
      ],
      'index' => env('ELASTIC_INDEX')
  ]
],


'indices' => [
  'my_index_1' => [
      "aliases" => [
          "my_index"
      ],
      'settings' => [
          "number_of_shards" => 1,
          "number_of_replicas" => 0,
      ],
      'mappings' => [
          'posts' => [
                'properties' => [
                    'title' => [
                        'type' => 'string'
                    ]
                ]
          ]
      ]
  ]
],

'indices' => [
  'rakkle_search' => [
      "aliases" => [
          "my_index"
      ],
      'settings' => [
          "number_of_shards" => 1,
          "number_of_replicas" => 0,
      ],
      'mappings' => [
          'posts' => [
                'properties' => [
                    'title' => [
                        'type' => 'string'
                    ]
                ]
          ]
      ]
  ]
]



//EOF
];