<?php

return [
    'driver' => env('SCOUT_DRIVER'),

    'queue' => env('SCOUT_QUEUE'),

    'es' => [
      'connection' => env('ELASTIC_CONNECTION'),
  ],
];