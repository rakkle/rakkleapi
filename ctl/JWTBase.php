<?php

namespace CTL;

use Lcobucci\JWT\Claim;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Illuminate\Support\Facades\Redis as rdstore;

class JWTBase {


    /**
     * Creates new tokens for returning users
     * @param  int      $uid
     * @param  array    $user
     * @return string
     */
    public function buildJWTtoken($uid, $user){
    $signer = new Sha256();
    //Creating Token
    $token = (new Builder())->setIssuer('rakkleservice')
            ->setAudience('http://rakkle.io')
            ->setId('auth'.$uid, true)
            ->setIssuedAt(time())
            ->setNotBefore(time() + 60)
            ->setExpiration(time() + 3600)
            ->set('uid', $uid)
            ->set('user', $user)
            ->sign($signer, 'rakkleservice')
            ->getToken();

        return $token;
    }

    /**
     * Consumes imcoming token and returns set claim
     * @param  string $token
     * @return array
     */
    public function parseJWTClaim($token){
        $parse = (new Parser() )->parse((string) $token);

        if(empty($parse)){
            throw new Exception;
        }
        return $parse->getClaims();
    }

    /**
     * Validates Incoming tokens
     * @param  string $token
     * @return boolean
     */
    public function validateJWTtoken($token){
        $parse = (new Parser())->parse((string) $token);

        if(empty($parse)){

            throw new Exception;
        }

        /**
         * Major bug found when using token from jwt.io Would need to investigate furter
         */
        $data = new ValidationData();
        $data->setIssuer('rakkleservice');
        $data->setAudience('http://rakkle.io');
        $data->get('jti');
        $data->setCurrentTime(time() + 160);

        return $parse->validate($data);

    }

    /**
     * Saves Created Token
     * @param  int      $id
     * @param  string   $token
     * @return void
     */
    public function saveToken($id, $token){
        rdstore::set('jwtToken'.':'.$id, $token);
    }

    /**
     * Fetching Created Token
     * @return string
     */
    public function getSavedToken($id){
        return rdstore::get('jwtToken'.':'.$id);
    }

    /**
     * Removes old tokens
     * @return void
     */
    public function flushToken(){

    }

}