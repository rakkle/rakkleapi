<?php

namespace CTL;

use Illuminate\Support\Facades\Redis as rdstore;

class PetReview {


  /**
   * [postPetReview description]
   * @param  [type] $petlist_id [description]
   * @param  [type] $review     [description]
   * @return [type]             [description]
   */
  public function postPetReview($petlist_id, $review){
    rdstore::lpush('reviews'.':'.$petlist_id, json_encode($review));
  }

  /**
   * [postCompanyReview description]
   * @param  [type] $petlist_id [description]
   * @param  [type] $review    [description]
   * @return [type]            [description]
   */
  public function postCompanyReview($company_id, $review){
    rdstore::lpush('companyReview'.':'.$company_id, json_encode($review));
  }

  /**
   * [getCompanyReview description]
   * @param  [type] $petlist_id [description]
   * @return [type]            [description]
   */
  public function postReviewComments($review_id, $comment){
    rdstore::lpush('reviewComments'.':'.$review_id, json_encode($comment));
  }

  /**
   * [getPetReview description]
   * @param  [type] $petlist_id [description]
   * @return [type]            [description]
   */
  public function getPetReview($petlist_id){
    return rdstore::lrange('reviews'.':'.$petlist_id, 0, -1);
  }


  /**
   * [getCompanyReview description]
   * @param  [type] $petlist_id [description]
   * @return [type]            [description]
   */
  public function getCompanyReview($company_id){
    return rdstore::lrange('companyReview'.':'.$company_id, 0, -1);
  }


  /**
   * [getReviewComments description]
   * @param  [type] $petlistid [description]
   * @param  [type] $comment   [description]
   * @return [type]            [description]
   */
  public function getReviewComments($review_id){
    return rdstore::lrange('reviewComments'.':'.$review_id, 0, -1);
  }



}