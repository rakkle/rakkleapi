<?php

namespace CTL;


class SearchBase{


  /**
   * Filter for users specific search queries
   * @param  array $token
   * @return array
   */
  public function filter($token){

    // Gather all input content within token
    /**
     * Token should have following
     * "pet_listing_name": "",
        "pet_name":"",
        "priceMin":"",
        "priceMax":"",
        "pet_sex":"",
        "pet_location":"",
        "pet_dob_from":"",
        "pet_dob_to":"",
        "pet_type":"",
        "pet_breed":"",
        "pet_size":""
     * e.g: $getToken = json_decode(json_encode($token['pet_listing_name']), true);
     */


      // $getListingName = json_decode(json_encode($token['pet_listing_name']), true);
      $getName = json_decode(json_encode($token['pet_name']), true);
      $getPriceMin = json_decode(json_encode($token['priceMin']), true);
      $getPriceMax = json_decode(json_encode($token['priceMax']), true);
      $getSex = json_decode(json_encode($token['pet_sex']), true);
      $getLocation = json_decode(json_encode($token['pet_location']), true);
      $getDOBFrom = json_decode(json_encode($token['pet_dob_from']), true);
      $getDOBTo = json_decode(json_encode($token['pet_dob_to']), true);
      $getType = json_decode(json_encode($token['pet_type']), true);
      $getBread = json_decode(json_encode($token['pet_breed']), true);
      $getSize = json_decode(json_encode($token['pet_size']), true);


    $query = \DB::table('pet_listings');

      // if(!empty($getListingName)){
      //  $search =  $query->where('pet_listing_name', 'LIKE', '%'.$getListingName.'%');
      // }

      if(!empty($getName)){
        $search = $query->where('pet_name', 'LIKE', '%'.$getName.'%');
      }

      if(!empty($getPriceMax)){
        $search = $query->whereBetween('price',[$getPriceMin, $getPriceMax] );
      }

      if(!empty($getSex)){
        $search = $query->where('pet_sex', '=', $getSex);
      }

      if(!empty($getLocation)){
        $search = $query->where('pet_location', 'LIKE', '%'.$getLocation.'%');
      }

      if(!empty($getDOBTo)){
        $search = $query->whereBetween('pet_dob', [$getDOBFrom, $getDOBTo]);
      }

      if(!empty($getType)){
        $search = $query->where('pet_type', 'LIKE', '%'.$getType.'%');
      }

      if(!empty($getBread)){
        $search = $query->where('pet_breed', 'LIKE', '%'.$getBread.'%');
      }

      if(!empty($getSize)){
        $search = $query->where('pet_size', 'LIKE', '%'.$getSize.'%');
      }

      return $search;
  }


  // EOF
}