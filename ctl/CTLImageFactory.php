<?php

namespace CTL;

use Aws\S3\S3Client;
use Aws\Laravel\AwsFacade as AWS;
use Aws\S3\Exception\S3Exception as Exception;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Aws\Common\Exception\MultipartUploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CTLImageFactory{

  protected $aws;

  public function __construct(){
    $this->aws = AWS::createClient('s3');
  }

  /**
   * Uploads user avatar
   * @param  UploadedFile $image
   * @param  int       $id
   * @return string
   */
  public function userAvatarUpload(UploadedFile $image, $id){

    $OrgImg = $image->getClientOriginalName();
    $ImageName = 'Avatar_'.$id.$OrgImg;
    $ImagePath = 'userAvatar/';


    $imageURL = $this->aws->putObject([
      'Bucket' => env('AWS_S3_BUCKET'),
      'Key' => $ImagePath.$ImageName,
      'ContentType' => $image->getClientMimeType(),
      'SourceFile' => $image,
      'ACL' => 'public-read'
    ]);


    return $imageURL['ObjectURL'];

  }

  /**
   * Uploads a new image to s3
   * @param  UploadedFile        $image
   * @param  int                 $id
   * @return string
   */
  public function petlistingImgUpload(UploadedFile $image, $id){
    $OrgImg = $image->getClientOriginalName();
    $ImageName = $id.'-'.$OrgImg;
    $ImagePath = 'petlistings/'.$id.'/';


    $imageURL = $this->aws->putObject([
      'Bucket' => env('AWS_S3_BUCKET'),
      'Key' => $ImagePath.$ImageName,
      'ContentType' => $image->getClientMimeType(),
      'SourceFile' => $image,
      'ACL' => 'public-read'
    ]);

    return $imageURL['ObjectURL'];
  }


  /**
   * Deletes Object from s3 (Incomplete)
   * @param  [type] $id         [description]
   * @param  [type] $listing_id [description]
   * @return [type]             [description]
   */
  public function deletePetlistingImg($id, $listing_id){
   $retrieveImg = $this->aws->getIterator('ListObjects', [
      "Bucket" => env('AWS_S3_BUCKET'),
      "Prefix" => 'petlistings/'.$listing_id.'/'
    ]);

   // $this->aws->copyObject([
   //    'Bucket' => env('AWS_S3_BUCKET');
   //    'CopySource' =>
   // ]);



   dd($retrieveImg);
  }




}